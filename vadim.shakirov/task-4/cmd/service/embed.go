//go:build embed
// +build embed

package main

import (
	_ "embed"
	"fmt"
)

//go:embed comms.txt
var comms_txt string

func init() {

	fmt.Println("Debug enabled")

	fmt.Println("comms.txt:\n", comms_txt)
}
