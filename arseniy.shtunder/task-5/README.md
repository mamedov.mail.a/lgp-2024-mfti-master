### Testing (testing.T)
```
go test
go test -v #verbose
go test -cover #analyze test coverage 
go test -run=<regex>
```

### Benchmark (testing.B)
```
go test -bench=.
go test -bench=<regex>
go test -bench-. -v
```

### Fuzzing (testing.F)
NO: Must be only one fuzz target per fuzz test
```
go test -fuzz <name>
go test -fuzz <name> -v
```

### Mockery
To install: 
```bash 
go install github.com/vektra/mockery/v2@v2.43.1 
```

To generate mocks for golang interfaces:
```
mockery --all --testonly --quiet --outpkg wifi_test --output .
```

### Test Coverage Analysis
```
go test -coverprofile=profile
go tool cover -html=profile -o coverage.html

```
### GolangCI Lint
To install: 
```bash
curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -
s -- -b $(go env GOPATH)/bin v1.50.1
```

```
golangci-lint run ./...
golangci-lint run file.go[or dir_name]
golangci-lint run --disable-all -E errcheck -E funlen 	#enable only errcheck and funlen
```
To launch linter using config file _golangci.yml_:
```
golangci-lint run --config=(your_path)/.golangci.yml
```